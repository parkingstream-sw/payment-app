import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:payment_app/classes/user.dart';
import 'package:payment_app/classes/timestamp.dart';
import 'package:payment_app/classes/vehicle.dart';
import 'package:payment_app/classes/payment.dart';
import 'package:payment_app/classes/stopwatch.dart';
import 'package:payment_app/themes.dart';

import 'package:payment_app/pagesFunctionality/addCreditCard.dart';
import 'package:payment_app/pages/splash.dart';
import 'package:payment_app/pages/home.dart';
import 'package:payment_app/pagesFunctionality/startTime.dart';
import 'package:payment_app/pagesFunctionality/stopTime.dart';
import 'package:payment_app/pagesFunctionality/addPayment.dart';
import 'package:payment_app/pagesFunctionality/addVehicle.dart';
import 'package:payment_app/pagesUser/userWallet.dart';
import 'package:payment_app/pages/country.dart';
import 'package:payment_app/pages/test.dart';
import 'package:payment_app/pages/settings.dart';
import 'package:payment_app/pagesUser/userLogin.dart';
import 'package:payment_app/pagesUser/userRegister.dart';
import 'package:payment_app/pagesUser/userProfile.dart';
import 'package:payment_app/pagesUser/userPic.dart';
import 'package:payment_app/pagesUser/userVehicles.dart';
import 'package:payment_app/pages/termsOfUse.dart';


void main() {

  runApp(
      MultiProvider(
        providers: [
          ChangeNotifierProvider<TimeStamp>(create: (context) => TimeStamp()),
          ChangeNotifierProvider<MyVehicles>(create: (context) => MyVehicles()),
          ChangeNotifierProvider<UserId>(create: (context) => UserId()),
          ChangeNotifierProvider<Wallet>(create: (context) => Wallet()),
          ChangeNotifierProvider<StopWatchService>(create: (context) => StopWatchService()),
     //     ChangeNotifierProvider(create: (context) => SomeOtherClass()),
        ],

        child: MaterialApp(
          title: 'Stream Pay',
          theme: myTheme(),

              //  initialRoute: '/wallet',
          routes: {
            '/':              (context) => Splash(),
            '/home':          (context) => Home(),
            '/start':         (context) => StartTime(),
            '/stop':          (context) => StopTime(),
            '/AddVehicle':    (context) => AddVehicle(),
            '/addPayment':    (context) => AddPayment (),
            '/addCreditCard': (context) => AddCreditCard (),
            '/wallet':        (context) => myWallet (),
            '/country':       (context) => Country (),
            '/profile':       (context) => Profile (),
            '/settings':      (context) => Settings (),
            '/test':          (context) => Test (),
            '/login':         (context) => Login (),
            '/register':      (context) => Register (),
            '/terms':         (context) => Terms (),
            '/pic':           (context) => PicSelect (),
            '/vehicles':      (context) => UserVehicles (),


          },

        ),
    ));


}
