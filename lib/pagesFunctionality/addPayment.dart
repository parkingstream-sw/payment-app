import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:payment_app/pagesFunctionality/addCreditCard.dart';

class AddPayment extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Payment Method"),
        centerTitle: true,),
      body: Center(

        child: Padding(
          padding: const EdgeInsets.fromLTRB(8, 40, 8, 8),
          child: Column(children: [
            Text('Select Prefered Method of Payment', style: Theme.of(context).textTheme.headline6,),
            SizedBox(height: 20),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
            children: [
              IconButton(
                      icon: Image.asset('assets/payment/visa-mastercard.png'),
                      iconSize: 100,
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => AddCreditCard()),
                        );
                      },
                  ),
              IconButton(
                      icon: Image.asset('assets/payment/google.png'),
                      iconSize: 100,
                      onPressed: () {},
                  ),
              IconButton(
                      icon: Image.asset('assets/payment/paypal.png'),
                      iconSize: 170,
                      onPressed: () {},
                  ),
              IconButton(
                      icon: Image.asset('assets/payment/apple.png'),
                      iconSize: 170,
                      onPressed: () {},
                  ),
                ])
          ],),
        ),
      ),
    );
  }
}
