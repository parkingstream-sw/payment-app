import 'package:flutter/material.dart';
import 'package:flex_color_picker/flex_color_picker.dart';
import 'dart:ui';
import 'package:flutter/rendering.dart';



class AddCreditCard extends StatefulWidget {

  @override
  _AddCreditCardState createState() => _AddCreditCardState();
}


class _AddCreditCardState extends State<AddCreditCard> {
  late Color screenPickerColor;
  late Color dialogPickerColor;
  late Color _creditCardColor=Colors.blue;

  @override
  void initState() {
    screenPickerColor = Colors.blue;
    dialogPickerColor = Colors.red;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: const Text('Credit Card'),
        ),
        body: Column(
          children:[
            SingleChildScrollView(
              padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  // Show the color picker in sized box in a raised card.
                  SizedBox(
                    width: double.infinity,
                    child: Card(
                      elevation: 2,
                      child: ColorPicker(    //Adds automatically a padding of 8
                        enableShadesSelection: false,

                        color: screenPickerColor,
                        onColorChanged:(Color color){
                          screenPickerColor = color;
                          _creditCardColor = screenPickerColor;
//                          setState((){});
                        },
                        width: 44,
                        height: 44,
                        borderRadius: 22,
                        heading: Text(
                          'Select CreditCard\'s color',
                          style: Theme.of(context).textTheme.headline6,
                        ),

                      ),
                    ),
                  ),
                ],
              ),
            ),
          ]
        )

    );
  }

}