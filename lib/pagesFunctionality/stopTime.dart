import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:payment_app/classes/timestamp.dart';
import 'package:provider/provider.dart';
import 'package:payment_app/functions.dart';

class StopTime  extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
  return Scaffold(
      body: DefaultTabController(
        initialIndex: Provider.of<TimeStamp>(context, listen:false).DurationMode?0:1,
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            title: Text('Set Stop Time'),
              centerTitle: true,
            bottom: TabBar(
              tabs: <Widget>[
                Tab(
                  text: "Set Duration",
                  icon: Icon(Icons.hourglass_top),
                ),
                Tab(
                  text: "Schedule end Time",
                  icon: Icon(Icons.today),
                ),
              ],
            ),
          ),
          body: TabBarView(
            children: <Widget>[
              Column(children: [
                CupertinoSetDuration(),
                _PrintStopTimeDataState()
              ],),
              Column(children: [
                CupertinoSetStopTime(),
                _PrintStopTimeDataState()
              ],),

            ],
          ),
        ),
      )
    );
  }
}


class CupertinoSetDuration extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(8),
      height: 250,
      child: Card(
        elevation: 4,
        child: Consumer<TimeStamp>(builder: (context, TimeStamp, child) =>
          Column(
            children: <Widget>[

              Expanded(
                child: CupertinoTimerPicker(
                  mode: CupertinoTimerPickerMode.hm,
                  initialTimerDuration: TimeStamp.TargetDuration,
             //     minuteInterval: 1,
                  //     use24hFormat: true,
                  onTimerDurationChanged: (data) {   ///storing the data output to the scheduledStartDate variable
                    TimeStamp.TargetDuration=data;
                  },
                ),
              ),

              Container(
                padding: EdgeInsets.all(8),
                width: double.infinity,
                child: ElevatedButton (
                  child: Text("Set Duration"),
                  onPressed: () {
   //                 TimeStamp.DurationMode = true;
                    Navigator.pop(context);
                    ScaffoldMessenger.of(context)
                      ..removeCurrentSnackBar()
                      ..showSnackBar(SnackBar(content: Text("Duration is Set to  ${Duration2StringHHmm(TimeStamp.TargetDuration)}"
                      "Press (P) to start the session")));
                  },
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }
}

class CupertinoSetStopTime extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<TimeStamp>(builder: (context, TimeStamp, child) =>
      Container(
          padding: EdgeInsets.all(8),
          height: 300,
          child: Card(
          elevation: 4,
            child: Column(
            children: <Widget>[
            //        Text("Schedule Parking", style: Theme.of(context).textTheme.headline6,),
                TextButton.icon(
                  label: Text("Schedule Parking", style: Theme.of(context).textTheme.headline6,),
                  icon: Icon(Icons.alarm_add, color: Colors.blue, size: 30,),
                  onPressed: (){},
                ),
                Expanded(
                  child: CupertinoDatePicker(
                  mode: CupertinoDatePickerMode.dateAndTime,
                  minimumDate: DateTime.now(),
                  initialDateTime: TimeStamp.StopTime, //TimeStamp.StartTime.add(TimeStamp.TargetDuration),
                  //     use24hFormat: true,

                  onDateTimeChanged: (data) {   ///storing the data output to the scheduledStartDate variable
                  //  TimeStamp.TargetDuration = data.difference(TimeStamp.StartTime);
                  },
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(8),
                  width: double.infinity,
                  child: ElevatedButton (
                    child: Text("Schedule"),
                    onPressed: () {
                      TimeStamp.DurationMode = false;
                      Navigator.pop(context);
                      ScaffoldMessenger.of(context)
                        ..removeCurrentSnackBar()
                        ..showSnackBar(SnackBar(content: Text("Stop Time is Set at  ${Date2String(TimeStamp.StopTime)}. "
                            "Press (P) to start the session" )));
                    },
                  ),
            ),
            ],
            ),
          ),
),
    );

  }
}







//class that displays the information  using  a listView
class _PrintStopTimeDataState extends StatelessWidget {

 final List<int> colorCodes = <int>[300,400,500,600];
 final  List<String> listText =
          ["Start time:",
            "Stop Time",
            "Duration:",
            "Total Rate"];

 List<String> makeList(DateTime startT, DateTime stopT, Duration totalDuration, double rate){
   List<String> varList = [
     Date2String(startT),
     Date2String(stopT),
     Duration2StringHHmm(totalDuration),
     rate.toString()+"€"
   ];
   return varList;
 }

 double rateCalc(Duration totoalTime){
   double _rate  = 1.5;  //rate per hour

   return totoalTime.inMinutes*_rate/60;
 }

  @override
  Widget build(BuildContext context) {
    return Consumer<TimeStamp>(builder: (context, TimeStamp, child) =>
       Container(
          height: 200,
          child:

          ListView.builder(
              padding: const EdgeInsets.all(8),
              itemCount: colorCodes.length,
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  height: 50,
                  color: Colors.lightGreen[colorCodes[index]],
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text("${listText[index]}:", style: TextStyle(color: Colors.blue),),
                        Text(makeList(TimeStamp.StartTime, TimeStamp.StopTime,TimeStamp.TargetDuration,rateCalc(TimeStamp.TargetDuration))[index])
                      ]),
                );
              }
          )


      ),
    );
  }
}
