import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:flex_color_picker/flex_color_picker.dart';
import 'package:payment_app/classes/vehicle.dart';
import 'package:provider/provider.dart';

  class AddVehicle extends StatefulWidget {

  @override
  _AddVehicleState createState() => _AddVehicleState();
}

class _AddVehicleState extends State<AddVehicle> {

    //vehicle_to_add stores the data from this page. it is defined in vehicle.dart
     Vehicle _vehicle = Vehicle(country: "GR", plate: "", color: Colors.green, type: VehicleType.Mini);

     ///cotroller for receiving the license plate data. This should be assignesd to vehicle_to_add.plate
     final _controller = TextEditingController( text: "ABC123");
     ///global validation key for FORM field
     final _formKey = GlobalKey<FormState>();
     FocusNode _plateFocusNode = FocusNode();


  // for the vehicle selection buttons
   //  final  List<AutovalidateMode>= [];
     List<Color> color_iconButton = List.filled(5,Colors.grey);


     List<String> list_iconText = ["Moto", "Mini", "Normal", "SUV", "+Size"  ];

  //Variables for clor picker
  late Color screenPickerColor;
  late Color dialogPickerColor;

  //variables for lisence palte

  final double plateWidth = 200; //lisence plate width
  String plateNumber = 'ZKO7759';

  //variables for countrycode and flags
  String countryFlag = 'packages/country_code_picker/flags/gr.png'; //complete path of a flag asset. initialized to the path of the flag of greece
  CountryCode selectedCountry = CountryCode(name:'Greece', flagUri: 'flags/gr.png', code:'GR', dialCode: '+30') ;  //initializing country variable
  final List <String> euCountries = <String>['BE','BG','CZ','DK','DE','EE',   //list of EU countries. for all of them we are going to use the EU FLAG and not the country one
    'IE','GR','ES','FR','HR','IT','CY','LV','LT','LU','HU','MT','NL','AT','PL',
    'PT','RO','SI','SK','FI','SE'];


  @override
  void initState() {
    screenPickerColor = Colors.blue;
    dialogPickerColor = Colors.red;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('New Vehicle'),
       // primary: false,

      ),

      body: Scrollbar(
        child: SingleChildScrollView(
          child: Column(children: [

            //Main Data entry
            SizedBox(   //plate
              width: double.infinity,
              child: Padding(padding: const EdgeInsets.all(8.0),
                child: Card(
                  elevation: 2,
                  child: Column(children: [
                      Text('Enter Registration Data', style: Theme.of(context).textTheme.headline6,),
                      Padding( padding: EdgeInsets.fromLTRB(10, 10,10, 0),
                        child: Row(children:[
                          Flexible(
                            flex:1,
                            fit: FlexFit.tight,
                            child: SizedBox(
                              width: 400, height: 60,
                              child: Padding( padding: const EdgeInsets.all(8.0),
                                child: CountryCodePicker(
                                  onChanged: (picked_country) {
                                    setState(() {
                                    // print('----');
                                    // print('old: ${selectedCountry}');
                                    // print(picked_country.toLongString());
                                    // print('uri: ${picked_country.flagUri}');
                                    selectedCountry=picked_country;
                                    _vehicle.country = selectedCountry.code!;
                                    if(euCountries.contains(selectedCountry.code)) { //countryFlag = 'packages/country_code_picker/'+selectedCountry.flagUri;
                                      countryFlag = 'assets/logo/EUFlag.png';
                                      print('EU code');
                                    }
                                    else
                                      countryFlag = 'packages/country_code_picker/'+selectedCountry.flagUri.toString();
                                    });
                                  },
                                  initialSelection: 'GR',
                                  showCountryOnly: true,
                                  showOnlyCountryWhenClosed: true,
                                  favorite: ['+30', 'GR'],
                                ),
                              ),
                            ),
                          ),
                          Flexible(
                            flex:1,
                            fit: FlexFit.tight,
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
                              child: plateInput(),
                            ),
                          ),
                        ]),
                    ),
                    SizedBox(height:10 ,),
                  ]),
                ),
              ),
            ),

            SizedBox(
              width: double.infinity,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Card(
                  elevation: 2,
                  child: selectSize(),
                ),
              ),
            ),

//color Picker
            SingleChildScrollView(
              padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                 // Show the color picker in sized box in a raised card.
                  SizedBox(
                    width: double.infinity,
                    child: Card(
                      elevation: 2,
                      child: ColorPicker(    //Adds automatically a padding of 8
                        enableShadesSelection: false,

                        color: screenPickerColor,
                        // onColorChanged: (Color color) =>
                        //     setState(() {
                        //       screenPickerColor = color;
                        //       vehicle_to_add.color = screenPickerColor;
                        //       print(vehicle_to_add.color);
                        //       print(vehicle_to_add.country);
                        //       print(vehicle_to_add.type);
                        //       }),
                        onColorChanged:(Color color){
                          setState((){
                              screenPickerColor = color;
                              _vehicle.vehicleColor = screenPickerColor;
                              // print(_vehicle.color);
                              // print(_vehicle.country);
                              // print(_vehicle.type);
                              for(int i=0; i< color_iconButton.length;i++) {
             //                  print(color_iconButton[i]);
                                if (color_iconButton[i] != Colors.grey)
                                  color_iconButton[i] = screenPickerColor;
                              }
                            });
                            },
                        width: 44,
                        height: 44,
                        borderRadius: 22,
                        heading: Text(
                          'Select color',
                          style: Theme.of(context).textTheme.headline6,
                        ),

                      ),
                    ),
                  ),
                ],
              ),
            ),

            //button
            Container(
              width: double.infinity,
              child: Padding( padding: const EdgeInsets.all(8.0),
                child: ElevatedButton (
                  child: Text("Save Vehicle"),
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      Provider.of<MyVehicles>(context,listen:false).addVehicle(_vehicle);// Validate returns true if the form is valid, otherwise false.
                      Navigator.pop(context,  _vehicle);
                       ScaffoldMessenger.of(context)
                         ..removeCurrentSnackBar()
                         ..showSnackBar(SnackBar(content: Text("Adding ${_vehicle.type} :" + _vehicle.country + " " + _vehicle.plate , style: TextStyle(color: _vehicle.vehicleColor),)));
                    }

                  },
                 ),
              ),
            ),
          ]
          ),
        ),
      ),
    );
  }



     Widget plateInput() {
       return     Form(
         key: _formKey,
         child: TextFormField(
           textAlign: TextAlign.start,
           style: TextStyle(color:Colors.blue, letterSpacing: 2, fontWeight: FontWeight.bold),
           decoration: InputDecoration(
               labelText: "plate",
               hintText: "ABC-1234",
               enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey, width: 1.0),)
           ),

            focusNode: _plateFocusNode,
            textCapitalization:TextCapitalization.characters,
        //   autofocus: true,
           textInputAction: TextInputAction.next,
           validator: (plate){
             Pattern patternPlate = r'^[a-zA-Z]{3}[-]?\d{4}$'; //3letters, perhaps "-" and 4 characters
             Pattern patternGR_en = r'^[aAbBeEzZHiIkKmMnNoOpPtTyYxX]{3}[-]?\d{4}$'; //patern for greek plates, english characters
             Pattern patternGR = r'^[αΑβΒεΕζΖηΗιΙκΚμΜνΝοΟπΠτΤυΥχΧ]{3}[-]?\d{4}$';  //patern for greek plates, greek characters
             RegExp regexPlate = new RegExp(patternPlate.toString());
             RegExp regexGR = new RegExp(patternGR_en.toString());
             if (!regexPlate.hasMatch(plate!) )
               return 'Invalid License Plate';
             else {
               _vehicle.plate = plate;
               return null;
             }
           },
           onSaved: (plate)=> _vehicle.plate = plate!,
           onFieldSubmitted: (_){
             fieldFocusChange(context, _plateFocusNode);
           },
         ));
     }


     Widget selectSize(){
    return  Column(children: [
       Text( "Select Vehicle size", style: Theme.of(context).textTheme.headline6,),
       Row(
       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
       crossAxisAlignment: CrossAxisAlignment.end ,
       children: [

        Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children:[
          IconButton(
             icon: Icon(vehicleIconMap[VehicleType.Moto],   color: color_iconButton[0],),

             onPressed: (){
               int button_index=0; //seting the 1st button
               if (color_iconButton[button_index]==Colors.grey) {
               for (int i = 0; i < color_iconButton.length; i++) //dissabling other buttons
               color_iconButton[i] = Colors.grey;
               color_iconButton[button_index] = screenPickerColor;  //asign to the icon the color of the colorpicker
               _vehicle.type = VehicleType.Moto;
               }
             else
              color_iconButton[button_index] = Colors.grey;
             setState(() {});
             },
             ),
         Text(list_iconText[0], style:TextStyle(color: Colors.grey))
            ]
             ),

        Column(children:[
           IconButton(
             icon: Icon(vehicleIconMap[VehicleType.Mini],   color: color_iconButton[1],),
             onPressed: (){
               setState(() {
               int button_index=1; //seting the 1st button
               if (color_iconButton[button_index]==Colors.grey) {
                for (int i = 0; i < color_iconButton.length; i++) //dissabling other buttons
                  color_iconButton[i] = Colors.grey;
                color_iconButton[button_index] = screenPickerColor;  //asign to the icon the color of the colorpicker
                _vehicle.type = VehicleType.Mini;
               }
              else
                color_iconButton[button_index] = Colors.grey;
              });
             },
             ),
          Text(list_iconText[1], style:TextStyle(color: Colors.grey))
          ]
         ),
        Column(children:[
          IconButton(
            icon: Icon(vehicleIconMap[VehicleType.Car],   color: color_iconButton[2], size: 32),
           onPressed: (){
             setState(() {
             int button_index=2; //seting the 1st button
             if (color_iconButton[button_index]==Colors.grey) {
                for (int i = 0; i < color_iconButton.length; i++) //dissabling other buttons
                  color_iconButton[i] = Colors.grey;
                color_iconButton[button_index] = screenPickerColor;  //asign to the icon the color of the colorpicker
                _vehicle.type = VehicleType.Car;
              }
              else
                color_iconButton[button_index] = Colors.grey;
           });
           },
           ),
              Text(list_iconText[2], style:TextStyle(color: Colors.grey))]
           ),

        Column(crossAxisAlignment: CrossAxisAlignment.end,
            children:[

          Container(
            height: 55,
            child: IconButton(
              icon: Icon(vehicleIconMap[VehicleType.SUV],   color: color_iconButton[3],size: 55),
             onPressed: (){
             setState(() {
             int button_index=3; //seting the 1st button
             if (color_iconButton[button_index]==Colors.grey) {
               for (int i = 0; i < color_iconButton.length; i++) //dissabling other buttons
                color_iconButton[i] = Colors.grey;
               color_iconButton[button_index] = screenPickerColor;  //asign to the icon the color of the colorpicker
               _vehicle.type = VehicleType.SUV;
               }
             else
              color_iconButton[button_index] = Colors.grey;
             });
           },
           ),
          ),
          Text(list_iconText[3], style:TextStyle(color: Colors.grey))]
       ),
        Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.end,
            children:[
          Container(
            height: 55,
            child: IconButton(
              icon: Icon(vehicleIconMap[VehicleType.Limo],   color: color_iconButton[4],size: 55),
               onPressed: (){
               setState(() {
               int button_index=4; //seting the 1st button
               if (color_iconButton[button_index]==Colors.grey) {
                  for (int i = 0; i < color_iconButton.length; i++) //dissabling other buttons
                      color_iconButton[i] = Colors.grey;
               color_iconButton[button_index] = screenPickerColor;  //asign to the icon the color of the colorpicker
               _vehicle.type = VehicleType.Limo;
              }
              else
                 color_iconButton[button_index] = Colors.grey;
              });
              },
            ),
            ),
          Text(list_iconText[4], style:TextStyle(color: Colors.grey))]
       ),
       ]
       ),
       ]);
     }

}


void fieldFocusChange(BuildContext context, FocusNode currentFocus) {
  currentFocus.unfocus();
 // FocusScope.of(context).requestFocus(nextFocus);
}
