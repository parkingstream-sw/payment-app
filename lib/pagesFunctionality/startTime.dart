import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:payment_app/classes/timestamp.dart';
import 'package:provider/provider.dart';
import 'package:payment_app/functions.dart';

class StartTime extends StatelessWidget {
  @override

  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Set Start Time'),
          centerTitle: true, ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[

          TopSwitch(),

          if(!Provider.of<TimeStamp>(context).SetStartTimeAutomatically )
            CuprtinoStartTimer(),

          if(Provider.of<TimeStamp>(context).SetStartTimeAutomatically )
            AutoStartButton(),

          InstructionRowBottom(),

            ])
    );
  }
}

class TopSwitch extends StatelessWidget {
  // const TopSwitch({
  //   Key key,
  // }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<TimeStamp>(builder: (context, TimeStamp, child) =>
      SwitchListTile.adaptive(
        title: Text('Set Start Parking Time automatically'),
        subtitle: Text('disable if you need some scheduling'),
      //  value: _setTimeAutomatically,
        value: TimeStamp.SetStartTimeAutomatically,
        onChanged: (val) {  //setting Start time to 1 min in the future as minimum distance feature for the cupertino timer

          DateTime _startTimeMinimum = DateTime.now().add(Duration(minutes:1));

          if(!val)
            if(TimeStamp.StartTime.compareTo(_startTimeMinimum)<0)
              TimeStamp.StartTime =_startTimeMinimum;

          print("new Start Time: ${TimeStamp.StartTime} ");
          TimeStamp.SetStartTimeAutomatically=val;
        },
      ),
    );
  }
}


class CuprtinoStartTimer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<TimeStamp>(builder: (context, TimeStamp, child) =>
            Container(
              padding: EdgeInsets.all(8),
              height: 300,
              child: Card(
                elevation: 4,
                child: Column(children:[

                    TextButton.icon(//i used textbutton only for graphical reasons. no need to use the callback
                      label: Text("Schedule Parking", style: Theme.of(context).textTheme.headline6,),
                      icon: Icon(Icons.alarm_add, color: Colors.blue, size: 30,),
                      onPressed: (){},
                      ),

                   Expanded(
                    child: CupertinoDatePicker(
                      mode: CupertinoDatePickerMode.dateAndTime,
                      minimumDate: DateTime.now(),
                      initialDateTime: TimeStamp.StartTime,
                        use24hFormat: true,
                      onDateTimeChanged: (data) {   ///storing the data output to the scheduledStartDate variable
                        TimeStamp.StartTime=data;
                        print("cupertinoData: $data");
                        print("TimeStamp.StartTime: ${TimeStamp.StartTime}");
                        },
                      ),
                    ),

                  ScheduleButton(),
                ]
                ),
              ),
            ),
          );

          }
}

class AutoStartButton extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(8),
      width: double.infinity,
      child: ElevatedButton (
        child: Text("save"),
        onPressed: () {
          Provider.of<TimeStamp>(context, listen: false).SetStartTimeAutomatically = true;
          print("TimeStamp.StartTime: ${Provider.of<TimeStamp>(context, listen: false).StartTime}");
          print("SetStartTimeAutomatically: ${Provider.of<TimeStamp>(context, listen: false).SetStartTimeAutomatically}");
          Navigator.pop(context);
          ScaffoldMessenger.of(context)
            ..removeCurrentSnackBar()
            ..showSnackBar(SnackBar(content: Text("Start Time will start automatically when you will press the (P) button")));
        },
      ),
    );
  }
}

class ScheduleButton extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(8),
      width: double.infinity,
      child: ElevatedButton (
        child: Text("Schedule"),
        onPressed: () {
          DateTime _startT=Provider.of<TimeStamp>(context, listen: false).StartTime;
          print("TimeStamp.StartTime: $DateTime");
          print("SetStartTimeAutomatically: ${Provider.of<TimeStamp>(context, listen: false).SetStartTimeAutomatically}");
          Navigator.pop(context);
            ScaffoldMessenger.of(context)
              ..removeCurrentSnackBar()
              ..showSnackBar(SnackBar(content: Text("Start Time is Set to start at ${Date2String(_startT)}"
                  "Please press the (P) button to confirm")));
        },
      ),
    );
  }
}



class ScheduleButton2 extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(8),
      width: double.infinity,
      child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            ElevatedButton (
              child: Text("Reset"),
              onPressed: () {
                Provider.of<TimeStamp>(context, listen: false).SetStartTimeAutomatically=false;
                //  Provider.of<TimeStamp>(context, listen: false).StartTime = DateTime.now().add(Duration(minutes:10));

              },
            ),
            ElevatedButton (
              child: Text("Schedule"),
              onPressed: () {
                //      Provider.of<TimeStamp>(context, listen: false).StartTime = _tempStartTimeDate;
                print("TimeStamp.StartTime: ${Provider.of<TimeStamp>(context, listen: false).StartTime}");
                Navigator.pop(context);
              },
            ),
          ]
      ),
    );
  }
}



class InstructionRowBottom extends StatelessWidget {
  const InstructionRowBottom({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(children:[
      //   ResizeImage('assets/logo/logo_round_transparent.png', width: )
            Flexible(
                flex:4,
                child: Padding(
                        padding: const EdgeInsets.fromLTRB(16.0,16,0,0),
                        child:

                        Provider.of<TimeStamp>(context, listen: false).SetStartTimeAutomatically ?
                        Text('* Parking the Parking button starts a parking session', style: TextStyle(color: Colors.grey[700]),):
                        Text('* Pressing the Parking button schedules a future parking session', style: TextStyle(color: Colors.grey[700])),
                )),
            Flexible(
              flex:1,
              child: Padding(
                padding: const EdgeInsets.only(left:8.0),
                child: Image.asset('assets/logo/logo_round_transparent.png',   width: 50,
                  height: 50,
                  cacheWidth: 50,
                  cacheHeight: 50,
                ),
              ),
            ),
    ]);
  }
}


