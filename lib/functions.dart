///this function is making sure that 5 appears as 05 when it comes to printing time
String formatHhmm(int val) {
  return val.toString().padLeft(2, "0");
  // if (val>9)
  //   return val.toString();
  // else
  //   return(val + 100).toString().substring(1);
}



String DateDiff2String(DateTime startD, DateTime stopD){
//   print('start: $startD');
//   print('stop: $stopD');
// return "x1";
  const int _minutesPerDay = 1440; //minutes in a day
  int _remainingMinutes4Day = _minutesPerDay-60*startD.hour-startD.minute; //remening minutes till the end of the day

  int _deltaMinutes = stopD.difference(startD).inMinutes; //deference of start and stop time in minutes
  //   print('diff: ${_deltaMinutes}');
  //   print("remaining: $_remainingMinutes");
  //   print("diff: $_deltaMinutes");

  String _timeString = formatHhmm(stopD.hour)+":"+formatHhmm(stopD.minute);
  if(_deltaMinutes<0) {
    return "invalid data";
  }
  if(_deltaMinutes<_remainingMinutes4Day){
    return _timeString;}
  else{
    int extraDays = ((_deltaMinutes - _remainingMinutes4Day)/_minutesPerDay ).floor();
    return _timeString+" +"+extraDays.toString();
  }
}

String Date2String(DateTime val){
      return formatHhmm(val.hour)+":"+formatHhmm(val.minute);
}

String Duration2StringHHmm(Duration val){
  if(val.inMinutes<60)
    return "00:"+formatHhmm(val.inMinutes);
  else {
    int hours = (val.inMinutes / 60.0).floor();
    int minutes = val.inMinutes - hours * 60;
    return formatHhmm(hours) + ":" + formatHhmm(minutes);
  }
}


String Duration2StringAdjustableHHmmORmmss(Duration val){
  if(val.inMinutes<60)
    return formatHhmm(val.inMinutes )+":"+formatHhmm(val.inSeconds %60);
  else {
    int hours = (val.inMinutes / 60.0).floor();
    int minutes = val.inMinutes - hours * 60;
    return formatHhmm(hours) + ":" + formatHhmm(minutes);
  }
}

String enumValue(String  value) {
return value.toString().substring(value.toString().indexOf('.') + 1);
}