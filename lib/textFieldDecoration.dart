import 'package:flutter/material.dart';

InputDecoration FormInputDecoration(IconData icondata, String hint, String label) {
  return InputDecoration(
      icon: Icon(icondata),
      hintText: hint,
      labelText: label,
      errorStyle: TextStyle(color: Colors.red, wordSpacing: 5.0,),
      labelStyle: TextStyle(color: Colors.blue, letterSpacing: 1.3),
      hintStyle: TextStyle(letterSpacing: 1.3),
      contentPadding: EdgeInsets.all(8.0),
      // Inside box padding
      border: OutlineInputBorder(borderRadius: BorderRadius.circular(8))
  );
}