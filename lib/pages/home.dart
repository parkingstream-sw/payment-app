//import 'dart:html';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:payment_app/classes/vehicle_svg_icons.dart';


import 'package:payment_app/classes/user.dart';
import 'package:payment_app/classes/timestamp.dart';
import 'package:payment_app/classes/vehicle.dart';
import 'package:payment_app/classes/payment.dart';

import 'package:payment_app/functions.dart';
//import 'package:enum_to_string/enum_to_string.dart';
import 'package:payment_app/pagesFunctionality/addVehicle.dart';
import 'package:payment_app/pagesFunctionality/addPayment.dart';
import 'package:payment_app/pagesFunctionality/startTime.dart';
import 'package:payment_app/pagesFunctionality/stopTime.dart';
import 'package:payment_app/pagesUser/userWallet.dart';
import 'package:payment_app/pagesUser/userProfile.dart';
import 'package:payment_app/pagesUser/userLogin.dart';
import 'package:payment_app/pagesUser/userPic.dart';
import 'package:payment_app/pages/settings.dart';
import 'package:payment_app/pagesUser/userVehicles.dart';
import 'package:payment_app/pages/termsOfUse.dart';


class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {

    print(Provider.of<MyVehicles>(context, listen: false).listOfVehicles.length);
    print('hello mama');


    return Scaffold(key: scaffoldKey,
    //  extendBodyBehindAppBar: true,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,

      body:Stack( children:[
        Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: GoogleMap(
              initialCameraPosition: CameraPosition(target: LatLng(29.9792,31.1342), zoom: 16.0),
              zoomGesturesEnabled: false,
            )
        ),
        SafeArea(


          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column( children: [

              AppBar(
                toolbarHeight: 45,
                elevation: 4,
                //   primary: false,
                automaticallyImplyLeading: false,
                title: const Text('Search address', style: TextStyle(color: Colors.grey),),
                actions:[
           //       ImageIcon(AssetImage('assets/avatar/mustache.png'),),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: InkWell(
                        child: Image.asset(Provider.of<UserId>(context, listen: true).AvatarPath),
                        onTap: () {scaffoldKey.currentState?.openEndDrawer();},
                    ),
                  ),

                ],
                backgroundColor: Colors.white,
                titleTextStyle: TextStyle(color: Colors.grey),
                foregroundColor: Colors.grey[850],
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),

              ),
              SizedBox(height: 20,),
              Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children:[
                    DropDownVehicles2(),

                    DropDownPayments2(),


                    Container(
                        height: 45,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          //  border: Border.all(color: Colors.grey, width:1, ),
                          borderRadius: BorderRadius.circular(12),
                          boxShadow: [
                            new BoxShadow(
                              blurRadius: 3,
                              color: Colors.grey,
                            )
                          ],
                        ),
                        child: Row(children: [
                          //     padding: const EdgeInsets.all(15.0),
                          IconButton(
                              padding: EdgeInsets.fromLTRB(0, 5, 10, 5),
                              constraints: BoxConstraints(),
                              icon: Icon(Icons.location_pin, color: Colors.blue),
                              onPressed: (){
                                print("# vehicles: " + Provider.of<MyVehicles>(context, listen: false).listOfVehicles.length.toString() );
                                print(" user id:" + Provider.of<UserId>(context, listen: false).UserAvatar.toString() );
                                print(" HasStarted: ${Provider.of<TimeStamp>(context, listen: false).HasStarted}");


                             //   print("#colors: " + carColors.length.toString());
                              }
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(0, 0, 10, 0),
                            child: Text('New York'),
                          )]
                        )
                    ),

                  ]
              ),

            ],

            ),
          ),
        ),
      ]
      ),
      endDrawer: AppDrawer(),
      floatingActionButton:myFloatingButton(),
      bottomNavigationBar:  myNavBar(),//BotomNavX(),

    );
  }
}

class AppDrawer extends StatelessWidget {
  const AppDrawer({
    Key? key,
  }) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return  Drawer(
        child: ListView(
          padding: EdgeInsets.zero,

          children: <Widget>[
            Consumer<UserId>(builder: (context, UserId, child) =>
              InkWell(
                child: DrawerHeader(

                  decoration: BoxDecoration(
                    color: Colors.grey[400],
                     image:DecorationImage(
                         fit: BoxFit.none,
                         image:  AssetImage(
                             UserId.AvatarPath)
                     ),

                  ),
                  child: Text("User",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 24,
                  ),
                    ),
                ),
                onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => PicSelect()),),
              ),
            ),
            ListTile(
              leading: Icon(Icons.face, color: Colors.blue),
              title: Text('Profile', style: TextStyle( color: Colors.blue)),
              onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => Profile()),),
            ),
            ListTile(
              leading: Icon(Icons.account_balance_wallet, color: Colors.blue),
              title: Text('Wallet', style: TextStyle( color: Colors.blue)),
              onTap: () =>
               Navigator.push(context, MaterialPageRoute(builder: (context) => myWallet()),),
            ),
            ListTile(
              leading: Icon(Icons.directions_car_sharp, color: Colors.blue),
              title: Text('Vehicles', style: TextStyle( color: Colors.blue)),
              onTap: () =>
                  Navigator.push(context, MaterialPageRoute(builder: (context) => UserVehicles()),),
            ),
            ListTile(
              leading: Icon(Icons.timeline, color: Colors.blue),
              title: Text('History', style: TextStyle( color: Colors.blue)),
              onTap: () => {},
            ),
            ListTile(
              leading: Icon(Icons.settings, color: Colors.blue),
              title: Text('Settings', style: TextStyle( color: Colors.blue)),
              onTap: () =>
                  Navigator.push(context, MaterialPageRoute(builder: (context) => Settings()),),
            ),

            ListTile(
              leading: Icon(Icons.policy, color: Colors.blue),
              title: Text('Terms of Use', style: TextStyle( color: Colors.blue)),
              onTap: () =>
                  Navigator.push(context, MaterialPageRoute(builder: (context) => Terms()),),
            ),
            ListTile(
              leading: Icon(Icons.login, color: Colors.blue),
              title: Text('Login', style: TextStyle( color: Colors.blue)),
              onTap: () =>
                  Navigator.push(context, MaterialPageRoute(builder: (context) => Login())),
            ),
          ],
        ),
      )
    ;
  }
}


class myNavBar extends StatefulWidget {
  @override
  _myNavBarState createState() => _myNavBarState();
}

class _myNavBarState extends State<myNavBar> {

  _onTapUpDown(){
  Provider.of<TimeStamp>(context, listen:false).TimeLaplseDirectionUp=  !Provider.of<TimeStamp>(context, listen:false).TimeLaplseDirectionUp;
  setState(() {  });
  }


  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      color: Colors.white,
      //width:150,
      child: Consumer<TimeStamp>(builder: (context, TimeStamp, child) =>
        Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              myNavItem(condition:TimeStamp.SetStartTimeAutomatically,
                   icon1: Icons.alarm_on, icon2:Icons.schedule ,
                   string1: "auto",
                   string2: DateDiff2String(DateTime.now(), TimeStamp.StartTime),
                   label1: "start", label2: "start" ,
                   callBack: ()=> { Navigator.push(
                     context, MaterialPageRoute(builder: (context) => StartTime()),)}
                    ),

              myNavItem(condition:TimeStamp.SetStartTimeAutomatically,
                  icon1: Icons.timer, icon2:Icons.schedule ,
                  string1: Date2String(TimeStamp.StopTime),
                  string2: Duration2StringHHmm(TimeStamp.TargetDuration),
                  label1: "stop", label2: "duration" ,
                  callBack: ()=> { Navigator.push(
                    context, MaterialPageRoute(builder: (context) => StopTime()),)}
              ),


              GestureDetector(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children:[
                    myStreamBuilder1(),
                  Text(TimeStamp.TimeLaplseDirectionUp?"time lapse": "countdown" ,)
                ]),
                onTap: _onTapUpDown,

              ),

              myNavItem(condition:true,
                  icon1: Icons.info_rounded, icon2:Icons.info_rounded ,
                  string1: "5€",
                  string2: "5€",
                  label1: "price", label2: "price" ,
                  callBack: ()=> {
                    print("Start: ${TimeStamp.StartTime}"),
                    print("Stop: ${TimeStamp.StopTime}"),
                    print("Duration: ${TimeStamp.TargetDuration}"),
                    print("TimeLapsed: ${TimeStamp.TimeLapse}"),
                    print("DurationMode: ${TimeStamp.DurationMode}"),
                    print("AutoStart: ${TimeStamp.SetStartTimeAutomatically}"),
                  }
              ),


            ]
        ),
      ),
    );
  }
}

class BotomNavX extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return
      Consumer<TimeStamp>(
          builder: (context, TimeStamp, child) =>
        BottomNavigationBar(
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon:  Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(TimeStamp.SetStartTimeAutomatically?Icons.alarm_on : Icons.schedule, color: Colors.blue, size: 30,),
                    Text(TimeStamp.SetStartTimeAutomatically?
                    "Auto" : DateDiff2String(DateTime.now(), TimeStamp.StartTime))
                  ]),
              label: 'Start',
            ),
            BottomNavigationBarItem(
              icon:  Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(TimeStamp.DurationMode?Icons.timer:Icons.schedule, color: Colors.blue, size: 30,),
                    Text(TimeStamp.DurationMode?Duration2StringHHmm(TimeStamp.TargetDuration): Date2String(TimeStamp.StopTime)),
                  ]),
              label: TimeStamp.DurationMode? "Duration":"Stop",
            ),
            BottomNavigationBarItem(
              icon:  Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(TimeStamp.DurationMode?Icons.timer:Icons.schedule, color: Colors.blue, size: 30,),
                    Text(TimeStamp.DurationMode?Date2String(TimeStamp.StopTime):Duration2StringHHmm(TimeStamp.TargetDuration)),
                  ]),
              label: TimeStamp.DurationMode? "Duration":"Stop",
            ),


          ],
          currentIndex: 1,
        ),
      );
  }}


class myNavItem extends StatelessWidget {
  final bool condition;
  final IconData icon1, icon2;
  final String string1, string2, label1, label2;
  VoidCallback callBack;


  //requiring the list of todos
  myNavItem({Key? key,
    required this.condition,
    required this.icon1, required this.icon2,
    required this.string1, required this.string2,
    required this.label1, required this.label2,
    required this.callBack}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                IconButton(
                    padding: EdgeInsets.fromLTRB(0, 5, 10, 5),
                    constraints: BoxConstraints(),
                    icon: Icon(condition? icon1 :icon2, color: Colors.blue, size: 30,),
                    onPressed: callBack
                ),
                Text(condition? label1 :label2 ),
              ]),
          Text(condition? string1 :string2),
        ]
    );

  }
  }



class mySearchBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:   AppBar(
        centerTitle: true,
        elevation: 4,
     //   primary: false,
        title: const Text('New Vehicle'),


    ),
    );
  }
}



class myStreamBuilder1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<String>(
      stream: Provider.of<TimeStamp>(context, listen: false).getStopWatchStream(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Text("--:--",);
        } else if (snapshot.connectionState == ConnectionState.done) {
          return Text("Done !",);
        } else if (snapshot.hasError) {
          return Text(snapshot.error.toString(),);
        } else if (snapshot.hasData) {
          return Text('${snapshot.data}',);
        }
        else
          return Text('--:--',);  //investigate this...
      },
    );
  }
}


class myFloatingButton extends StatefulWidget {
  @override
  _myFloatingButtonState createState() => _myFloatingButtonState();
}

class _myFloatingButtonState extends State<myFloatingButton> {
  Color _color = Colors.blue;
  Widget _widget = Image.asset('assets/logo/logo_round_transparent.png');
  @override
  Widget build(BuildContext context) {

    return FloatingActionButton(
          elevation: 4,
          materialTapTargetSize: MaterialTapTargetSize.values[0],
          child: _widget,
          backgroundColor: _color,
          onPressed: (){
            if(!Provider.of<TimeStamp>(context, listen: false).HasStarted) {
              print("x1");
              Provider.of<TimeStamp>(context, listen: false).startSession();
              setState(() {
                _color = Colors.red;
                _widget = Icon(Icons.stop);
              });
            }
              else {
              print("x2");
              Provider.of<TimeStamp>(context, listen: false).stopSession();
              setState(() {
                _color = Colors.blue;
                _widget = Image.asset('assets/logo/logo_round_transparent.png');
              });
              }
            }
            );
    }

  }


class DropDownVehicles2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return Consumer<MyVehicles>(builder: (context, MyVehicles, child) =>
        Container(
          height: 45,
          padding: EdgeInsets.fromLTRB(5,0,0,0),
          decoration: BoxDecoration(
            color: Colors.white,
//  border: Border.all(color: Colors.grey, width:1, ),
            borderRadius: BorderRadius.circular(12),
            boxShadow: [
              new BoxShadow(
                blurRadius: 3,
                color: Colors.grey,
              )
            ],
          ),
          child: DropdownButtonHideUnderline (
            child: DropdownButton<Vehicle>(
              isDense: true,
              value: MyVehicles.listOfVehicles[MyVehicles.preferedVehicle],
              onChanged: (Vehicle? dropDownVehicle) {
                 if (dropDownVehicle?.plate=='ADD')
                   Navigator.push(context, MaterialPageRoute(builder: (context) => AddVehicle()),);
                 else
                  MyVehicles.PreferedVehicle =  MyVehicles.listOfVehicles.indexOf(dropDownVehicle!);

                print("new car : " + MyVehicles.preferedVehicle.toString() +  "string: " +MyVehicles.listOfVehicles[MyVehicles.preferedVehicle].plate);
              },

                items: (MyVehicles.listOfVehicles +[Vehicle(country: 'GR', plate:  'ADD',  color: Colors.grey, type: VehicleType.Add)]).map((Vehicle item) {
         //     items: MyVehicles.listOfVehicles.map((Vehicle item) {
                return DropdownMenuItem<Vehicle>(
                  child: item.type!=VehicleType.Add?
                      Row(
                        mainAxisSize: MainAxisSize.min,

                      children: [
                      Icon(vehicleIconMap[item.type], color: item.vehicleColor),
                      SizedBox(width: 5),
                      Text(item.plate, style: TextStyle(color: item.vehicleColor)),
                  ]):Center(child: Icon(vehicleIconMap[item.type], color: item.vehicleColor)),
                  value: item,
                );
              }).toList(),

            ),
          ),
        ));

  }
}


class DropDownPayments2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return Consumer<Wallet>(builder: (context, Wallet, child) =>
        Container(

          height: 45,
          decoration: BoxDecoration(
            color: Wallet.listMeansOfPayment[Wallet.defaultCard].color,
//  border: Border.all(color: Colors.grey, width:1, ),
            borderRadius: BorderRadius.circular(12),
            boxShadow: [
              new BoxShadow(
                blurRadius: 3,
                color: Colors.grey,
              )
            ],
          ),

          child: DropdownButtonHideUnderline (
            child: DropdownButton<PaymentMethod>(
              underline: null,
              isDense: true,
              value: Wallet.listMeansOfPayment[Wallet.defaultCard],
              onChanged: (PaymentMethod? dropDownPaymentMethod) {
                print(dropDownPaymentMethod?.type.toString());
                if (dropDownPaymentMethod?.type==PaymentType.Add)
                  Navigator.push(context, MaterialPageRoute(builder: (context) => AddPayment()),);
                else
                  Wallet.defaultCard =  Wallet.listMeansOfPayment.indexOf(dropDownPaymentMethod!);

                print("new car : " + Wallet.defaultCard.toString() +  "string: " +Wallet.listMeansOfPayment[Wallet.defaultCard].type.toString());
              },
         //     items: Wallet.listMeansOfPayment.map((PaymentMethod item) {

             items: (Wallet.listMeansOfPayment+ [PaymentMethod.alt(type: PaymentType.Add, expDate: DateTime(2025, 10, 31), name: " ")]).map((PaymentMethod item) {
                return DropdownMenuItem<PaymentMethod>(
                  child: Center(
                    child: Container(
                        width:50,
                        height: 24,
                        padding: EdgeInsets.fromLTRB(3, 1, 3, 1),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: item.color,

                        ),

                        child: (item.type ==PaymentType.Add)?
                                Icon(Icons.add_circle, color:Colors.grey):
                          Image.asset(paymentPicPathMap[item.type]!, height:24)),

                  ),
                  value: item,
                );
              }).toList(),

            ),
          ),
        ));

  }
}


