import 'package:payment_app/classes/vehicle.dart';
import 'package:payment_app/classes/user.dart';
import 'package:payment_app/classes/payment.dart';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:payment_app/pages/home.dart';
import 'package:provider/provider.dart';

class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {



  @override
  void initState() {
    Future.delayed(Duration(seconds:1), (){
      print ('3sec');
      Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (context) => Home()));
   //   Navigator.pushReplacementNamed(context, '/test', arguments: person);
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: SafeArea(
        child: Column(
          children: <Widget>[
            SizedBox(height: 50,),
            Container(child: Image.asset('assets/logo/logo2.png', scale: 5)),

            Text('Stream Pay', textAlign: TextAlign.center, style: TextStyle(color: Colors.blue, fontSize: 40) ),
            Expanded(child: SpinKitFoldingCube(color: Colors.blue, size: 40.0, )),
            Image.asset('assets/logo/houses.png')
          ],
        )
      ),

    );
  }
}
