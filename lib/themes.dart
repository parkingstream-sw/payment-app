import 'package:flutter/material.dart';

ThemeData myTheme () {

  return  ThemeData(
    // Define the default brightness and colors.
    brightness: Brightness.light,

    primaryColor: Colors.lightBlue[800],
    accentColor: Colors.cyan[600],

    // Define the default font family.
  //  fontFamily: 'IndieFlower',

    // Define the default TextTheme. Use this to specify the default
    // text styling for headlines, titles, bodies of text, and more.
    textTheme: TextTheme(
              headline1: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold, fontFamily: 'Logo'  ),   //used for splash screen
              headline6: TextStyle(fontSize: 20.0, color: Colors.blue), //Card Title in blue, top of a card
              headline5: TextStyle(fontSize: 20.0, color: Colors.black), //Price in a card, important data to show
              bodyText2: TextStyle(fontSize: 14.0, fontFamily: 'Hind'),

              )
  );
}