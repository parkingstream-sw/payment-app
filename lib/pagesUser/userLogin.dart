import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:payment_app/textFieldDecoration.dart';

import 'package:provider/provider.dart';
import 'package:payment_app/pagesUser/userProfile.dart';
import 'package:payment_app/classes/user.dart';
import 'package:payment_app/pages/termsOfUse.dart';



final _formKey = new GlobalKey<FormState>(); // outside the build() method

class Login extends StatelessWidget {



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Login'),
        // primary: false,

      ),
      body: Container(
        padding: EdgeInsets.all(30),

        child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [


          Column(

            children: [
              SizedBox(height:50),
              TextFormField(
              textCapitalization: TextCapitalization.none,
              decoration: FormInputDecoration(Icons.email, 'please enter your email address', 'email'),
              onSaved: (String? value) {
              },
              validator: (String? value) {
                return (value != null && value.contains('@')) ? 'Do not use the @ char.' : null;
              },
                ),

            SizedBox(height: 20),

            TextFormField(
            scrollPhysics: BouncingScrollPhysics(),
            obscureText: true,
            textCapitalization: TextCapitalization.none,
            decoration: FormInputDecoration(Icons.lock, 'please enter your password', 'password'),
            onSaved: (String? value) {
            },
            validator: (String? value) {
              return (value != null && value.contains('@')) ? 'Do not use the @ char.' : null;
            },
          ),
            Row(
            mainAxisAlignment: MainAxisAlignment.end,
              children: [TextButton(onPressed:(){}, child: Text("forgot password?"),)]),
            SizedBox(height: 40),

              Row (
                  children:[
                    Checkbox(
                      value: true,
                    onChanged:(bool? newValue){},
                    ),
                    Text("I accept the"),
                    TextButton(child: Text("Term and Conditions"),
                      onPressed:(){
                        Navigator.push(context, MaterialPageRoute(builder: (context) => Terms()),);

                      },
                    ),


                  ]),
              Row(
                  children:[
                    Expanded(
                        flex: 1,
                        child: OutlinedButton(
                          child: Text("Register"),
                          onPressed:() {
                            Provider.of<UserId>(context,listen: false).password= "qwe123";
                            Provider.of<UserId>(context,listen: false).email= "christos@gmail.com";
                            Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => Profile()),);
                          },
                        )
                    ),
                    SizedBox(width: 10,),

                    Expanded(
                      flex: 1,
                      child: ElevatedButton (
                        child: Text("Sign In"),
                        onPressed: () {
                          Navigator.pop(context);
                          //  if (_formKey.currentState.validate()) {
                          //    Provider.of<MyVehicles>(context,listen:false).addVehicle(_vehicle);// Validate returns true if the form is valid, otherwise false.
                          //   Navigator.pop(context,  _vehicle);
                          // ScaffoldMessenger.of(context)
                          //   ..removeCurrentSnackBar()
                          //   ..showSnackBar(SnackBar(content: Text("Adding Vehicle: " + _vehicle.country + " " + _vehicle.plate)));
                          //}

                        },
                      ),
                    ),
                  ]
              ),
            ]),


          Column(

              children: [
                Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [Text("Use Social Media", style: TextStyle(color: Colors.blue),)]),
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children:[
                  InkWell(
                    child: Container(
                      child: Image.asset('assets/social/fbook.png', height: 60)
                    ),
                    onTap: (){},
                  ),
                  InkWell(
                    child: Container(
                        child: Image.asset('assets/social/twitter.png', height: 60)
                    ),
                    onTap: (){},
                  ),
                  InkWell(
                    child: Container(
                        child: Image.asset('assets/social/linkedin.png', height: 60)
                    ),
                    onTap: (){},
                  ),

                  if(Theme.of(context).platform == TargetPlatform.android)
                    InkWell(
                      child: Container(
                          child: Image.asset('assets/social/google.png', height: 60)
                      ),
                      onTap: (){},
                    ),
                  if(Theme.of(context).platform == TargetPlatform.iOS)
                  InkWell(
                    child: Container(
                        child: Image.asset('assets/social/apple.png', height: 60)
                    ),
                    onTap: (){},
                  ),



            ]
          ),
              ]
          ),
        ]),
      )
    );
  }
}








class UserName extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Form(
      //  key: _formKey,
        child: TextFormField(
          textAlign: TextAlign.start,
          style: TextStyle(color: Colors.blue,
              letterSpacing: 2,
              fontWeight: FontWeight.bold),
          decoration: InputDecoration(
              labelText: "plate",
              hintText: "ABC-1234",
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.grey, width: 1.0),)
          ),

          //   focusNode: _plateFocusNode,
          textCapitalization: TextCapitalization.characters,
          //   autofocus: true,
          textInputAction: TextInputAction.next,
          // validator: (plate){
          //   Pattern patternPlate = r'^[a-zA-Z]{3}[-]?\d{4}$'; //3letters, perhaps "-" and 4 characters
          //   Pattern patternGR_en = r'^[aAbBeEzZHiIkKmMnNoOpPtTyYxX]{3}[-]?\d{4}$'; //patern for greek plates, english characters
          //   Pattern patternGR = r'^[αΑβΒεΕζΖηΗιΙκΚμΜνΝοΟπΠτΤυΥχΧ]{3}[-]?\d{4}$';  //patern for greek plates, greek characters
          //   RegExp regexPlate = new RegExp(patternPlate);
          //   RegExp regexGR = new RegExp(patternGR_en);
          //   if (!regexPlate.hasMatch(plate) )
          //     return 'Invalid License Plate';
          //   else {
          //     _vehicle.plate = plate;
          //     return null;
          //   }
          // },
          // onSaved: (plate)=> _vehicle.plate = plate,
          // onFieldSubmitted: (_){
          //   fieldFocusChange(context, _plateFocusNode);
          // },
        ));
  }
}
