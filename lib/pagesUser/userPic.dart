import 'package:flutter/material.dart';
import 'package:payment_app/classes/user.dart';
import 'package:provider/provider.dart';

class PicSelect extends StatelessWidget {
  String mypic='lipstick.png';
  void _picAction(String _picname){
    print(_picname);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('my Avatar'),
        // primary: false,

      ),
      body: GridView.count(
        primary: false,
        padding: const EdgeInsets.all(20),
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
        crossAxisCount: 3,
        children: <Widget>[
          PixBox(Avatar.genderX),
          PixBox( Avatar.heart),
          PixBox(Avatar.lipstick),
          PixBox(Avatar.mustache),
          PixBox(Avatar.nerd),
          PixBox(Avatar.plain),

          InkWell(
            onTap: () => {},
            child: Container(
              padding: const EdgeInsets.all(8),
              child: Icon(Icons.photo, size: 70, color: Colors.white),
                       color: Colors.blue,
            ),
          ),


        ],
      )
    );
  }
}





class PixBox extends StatelessWidget {
  PixBox( this.myAvatar);
  final Avatar myAvatar;

  @override
  Widget build(BuildContext context) {
    return  InkWell(
      onTap: () =>{ Provider.of<UserId>(context,listen:false).avatar=myAvatar,
      Navigator.pop(context),
      },
      //onTap: () => Provider.of<UserId>(context,listen:false).avatar=myAvatar,
      child: Container(
        padding: const EdgeInsets.all(8),
        child: Image.asset('assets/avatar/' + getAvatarName(myAvatar) + '.png'),
        color: Colors.grey[100],
      ),
    );
  }
}
