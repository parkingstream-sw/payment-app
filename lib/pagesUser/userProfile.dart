import 'package:flutter/material.dart';
import 'package:payment_app/textFieldDecoration.dart';

class Profile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('User Profile'),
      ),
        body: Column(
            children:[

              TextFormField(
                textCapitalization: TextCapitalization.none,
                decoration: FormInputDecoration(Icons.email, 'please enter your email address', 'email'),
                onSaved: (String? value) {
                },
                validator: (String? value) {
                  return (value != null && value.contains('@')) ? 'Do not use the @ char.' : null;
                },
              ),

            ]
        )
    );
  }
}
