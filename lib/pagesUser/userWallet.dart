import 'package:flutter/material.dart';
import 'package:payment_app/classes/payment.dart';
import 'package:provider/provider.dart';
import 'package:payment_app/pagesFunctionality/addPayment.dart';


class myWallet extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
        appBar: AppBar(
          title: Text("Wallet"),
          centerTitle: true,),

        body: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
//            mainAxisSize: MainAxisSize.min,

              children: [
                AccountBalance2(),
                Discounts(),
                 //cards
                 Container(
                  margin: EdgeInsets.symmetric(vertical: 20.0),
                  height: 260.0,
                   child: Consumer<Wallet>(builder: (context, Wallet, child) =>
                       ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: Wallet.listMeansOfPayment.length ,
                        itemBuilder: (context, index) {
                        return CardWidget(Wallet.listMeansOfPayment[index], index);
                      }
                  ),),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    width: double.infinity,
                    child: ElevatedButton(
                        child: Text("Add New Payment Method"),
                        onPressed: (){
                          Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => AddPayment()),);
                        }, ),
                  ),
                )
          ]),
        ),
    );
 }
}

class Discounts extends StatelessWidget {
  const Discounts({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(8),
        width: double.infinity,
        child: Card(
          elevation: 4,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(8, 4, 8, 4),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text("Discounts", style: Theme.of(context).textTheme.headline6,),
                  SizedBox( height: 20, ),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children:[
                        Text(Provider.of<Wallet>(context,listen:false).discounts.toString()+"€", style: Theme.of(context).textTheme.headline5,),
                        Icon(Icons.local_offer, color: Colors.red,),
                      ]),

                ]),
          ),)
    );
  }
}

class AccountBalance2 extends StatelessWidget {
  const AccountBalance2({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(8),
        width: double.infinity,
        //  height: 200,  --> look below   @mainAxisSize: MainAxisSize.min,

        child: Card(
          elevation: 4,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(8, 4, 8, 4),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text("Account Balance", style: Theme.of(context).textTheme.headline6,),
                  SizedBox( height: 20, ),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                    Text(Provider.of<Wallet>(context,listen:false).balance.toString()+"€", style: Theme.of(context).textTheme.headline5,),
                    ElevatedButton (
                      child: Text("Refill"),
                        onPressed: () {
                    },
                  ),
                  ])
                ]),
          ),)
    );
  }
}


Widget _payCard(PaymentMethod _method){
  return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Image.asset(_method.imagePath),
    );
}

Widget _creditCard(PaymentMethod _method){
  return
    Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children:[
                Container(
//                    clipBehavior: Clip.hardEdge,
                  //        color: Colors.grey,//image
                    height: 100,width:100,
                    child: Image.asset(_method.imagePath)
                ),
              ]
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(15, 8, 8, 8),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children:[
                  Text("**** **** **** "+_method.lastDigits.toString()),
                  Text(_method.cardName),
                ]
            ),
          )


        ])
  ;
}

class CardWidget extends StatelessWidget {
  CardWidget(this.pmethod, this.index);
  final PaymentMethod pmethod;
  final int index;

  @override
  Widget build(BuildContext context) {
    return
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Material(
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          elevation: 4,
          child: Container(

            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children:[
                 Consumer<Wallet>(builder: (context, Wallet, child) =>
                    Text(Wallet.defaultCard==index?"Default":" ", style: Theme.of(context).textTheme.headline6,)),
                    Container(
                    clipBehavior: Clip.antiAlias,
                    height: 160,
                    width: 250,

                    decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ],
                      color: pmethod.color,
                      //     border: Border.all(),
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    ),
                    child:  (pmethod.type==PaymentType.Mastercard || pmethod.type==PaymentType.Visa)?
                      _creditCard(pmethod): _payCard(pmethod),
),
                  SizedBox( height: 10, ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                    children:[
                      TextButton(
                        child: Text("Delete", style: TextStyle(color: Colors.grey),),
                        onPressed: (){

                          return showDialog(
                            context: context,
                            builder: (ctx) => AlertDialog(
                              title: Text("Delete Payment Method?"),
                              content: Text("Please Confirm"),
                              actions: <Widget>[
                                TextButton(
                                  onPressed: () {
                                    Provider.of<Wallet>(context,listen: false).deleteCard(index);
                                    Navigator.of(ctx).pop();
                                  },
                                  child: Text("Yes"),
                                ),
                                TextButton(
                                  onPressed: () {
                                    Navigator.of(ctx).pop();
                                  },
                                  child: Text("No"),
                                ),
                              ],
                            ),
                          );
                        },

                      ),

                     // SizedBox(width:20),
                      TextButton (
                        child: Text("Set Default"),
                        onPressed: () {
                          Provider.of<Wallet>(context,listen: false).defaultCard=index;

                        },
                      ),
                    ]
                ),

                ]),
          ),
        ),
      );
  }
}
