import 'package:flutter/material.dart';
import 'package:payment_app/classes/vehicle.dart';
import 'package:provider/provider.dart';
import 'package:payment_app/pagesFunctionality/addVehicle.dart';

// PaymentMethod Pm1 = PaymentMethod(type: PaymentType.mastercard , color: Colors.red, cardNumber:123456789456, lastDigits: 1234, expMonth: 02, expYear: 89, ccv: 123, name: "Christos");
// PaymentMethod Pmgoog = PaymentMethod(type: PaymentType.paypal);


class UserVehicles extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(
        title: Text("Vehicles"),
        centerTitle: true,),

      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
            // mainAxisSize: MainAxisSize.min,
            children: [

          //cards
          Container(
            margin: EdgeInsets.symmetric(vertical: 10.0),
            height: 350.0,
            child: Consumer<MyVehicles>(builder: (context, MyVehicles, child) =>
                ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: MyVehicles.listOfVehicles.length ,
                    itemBuilder: (context, index) {
                      return CardWidget(MyVehicles.listOfVehicles[index], index);
                    }
                ),),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              width: double.infinity,
              child: ElevatedButton(
                child: Text("Add New Vehicle"),
                onPressed: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => AddVehicle()),);
                }, ),
            ),
          ),


          Plate(),
        ]),
      ),
    );
  }
}


class CardWidget extends StatelessWidget {
  CardWidget(this.vehicle, this.index);
  final Vehicle vehicle;
  final int index;

  @override
  Widget build(BuildContext context) {
    return
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Material(
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          elevation: 4,
          child: Container(

            child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children:[
                  Consumer<MyVehicles>(builder: (context, MyVehicles, child) =>
                      Text(MyVehicles.preferedVehicle==index?"Default":" ", style: Theme.of(context).textTheme.headline6,)),

                  VehicledWidget(vehicle, index),

              //    SizedBox( height: 10, ),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children:[
                        TextButton(
                          child: Text("Delete"),
                          onPressed: (){

                            // return showDialog(
                            //   context: context,
                            //   builder: (ctx) => AlertDialog(
                            //     title: Text("Delete Vehicle?"),
                            //     content: Text("Please Confirm"),
                            //     actions: <Widget>[
                            //       TextButton(
                            //         onPressed: () {
                            //           Provider.of<MyVehicles>(context,listen: false).deleteVehicle(index);
                            //           Navigator.of(ctx).pop();
                            //         },
                            //         child: Text("Yes"),
                            //       ),
                            //       TextButton(
                            //         onPressed: () {
                            //           Navigator.of(ctx).pop();
                            //         },
                            //         child: Text("No"),
                            //       ),
                            //     ],
                            //   ),
                            // );
                          },

                        ),

                        // SizedBox(width:20),
                        ElevatedButton (
                          child: Text("Set Default"),
                          onPressed: () {
                            Provider.of<MyVehicles>(context,listen: false).PreferedVehicle=index;

                          },
                        ),
                      ]
                  ),

                ]),
          ),
        ),
      );
  }
}


class VehicledWidget extends StatelessWidget {
  VehicledWidget(this.vehicle, this.index);
  final Vehicle vehicle;
  final int index;

  @override
  Widget build(BuildContext context) {
    return Column(
            mainAxisSize: MainAxisSize.min,
            children:[
              Icon(vehicleIconMap[vehicle.type], size: 200, color: vehicle.vehicleColor),
              Text("${vehicle.country} ${vehicle.plate}"),
              SizedBox( height: 20, ),

            ]);
  }
}


class Plate extends StatelessWidget {
  @override

  final double _plateWidth = 300;
  final double _plateSizeRatio = 4.72;
  final int _sectionCountry=(45/520).floor();
  final int _sectionLetter=(47/520).floor();
  final int _sectionSymbol=(63/520).floor();


  Widget build(BuildContext context) {
    return Container(
      height:70,//_plateWidth/_plateSizeRatio,
      width:300, //_plateWidth,
      clipBehavior: Clip.hardEdge,
      decoration: BoxDecoration(
        border: Border.all(width: 3, color: Colors.black),
        borderRadius: BorderRadius.circular(12),
        boxShadow: [
          new BoxShadow(
            blurRadius: 1,
            color: Colors.white,
          )
        ],
      ),
      child: Row(children:[
                  Row(
                    children:[
                      Container(
                        color: Color.fromRGBO(0, 50, 153, 1),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children:[
                            //Flexible(flex:1,  child: Image.asset('assets/logo/EUFlag.png')),
                            Expanded(flex:1,  child: Image.asset('packages/country_code_picker/flags/eu.png'),),

                            Expanded(flex:1,
                                child: Center(
                                  child: Text("GR",
                                  style: TextStyle(fontFamily: 'Plate',
                                  color: Colors.white)),
                                )),
                          ]
                        ),
                      ),

                      FittedBox(
                          fit: BoxFit.fitHeight,
                          child: Text("ABC", style: TextStyle(fontFamily: 'Plate'))),

                      Container(
                        child: Text(" -  ")),

                      Container(
                        child: Text("1234", style: TextStyle(fontFamily: 'Plate'))),


            ]


          ),

])
    );
  }
}
