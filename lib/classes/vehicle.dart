import 'package:payment_app/classes/vehicle_svg_icons.dart';
import 'package:flutter/material.dart';

enum  VehicleType {   Moto, Mini, Car, SUV, Limo, Add }
///limo is 7 seater+

Map<VehicleType,IconData> vehicleIconMap  = {
  VehicleType.Moto: Icons.sports_motorsports,
  VehicleType.Mini: VehicleSvg.mini,
  VehicleType.Car:  Icons.directions_car ,
  VehicleType.SUV:  VehicleSvg.suv ,
  VehicleType.Limo:  VehicleSvg.limo ,
  VehicleType.Add:  Icons.add_circle
};
//   Icon(MdiIcons.carHatchback, size: 35),



class Vehicle{
  String _plate;
  String _country;
  Color _color;
  VehicleType _type;
  bool _preferedVehicle=true;

  Vehicle({required String country, required String plate, required Color color,required VehicleType type})
  : _country=country,
    _plate=plate,
    _color=color,
    _type=type;


  set vehicleColor(Color val) => _color=val;
  set type(VehicleType val) => _type=val;
  set plate(String val) => _plate=val;
  set country(String val) => _country=val;
  set preferedVehicle(bool val) => _preferedVehicle=val;

  Color get vehicleColor => _color;
  VehicleType get type => _type;
  String get plate => _plate;
  String get country => _country;
  bool get preferedVehicle => _preferedVehicle;
  IconData? get icon => vehicleIconMap[_type];
}

class MyVehicles extends ChangeNotifier {
  List<Vehicle> listOfVehicles = [];

  int _preferredVehicleIdx = -1; //

  void init() {
    Vehicle vehicle1 = Vehicle(country:  'GR', plate: 'ZKO7758',color: Colors.green, type: VehicleType.Car);
    Vehicle vehicle2 = Vehicle(country: 'IT', plate:'PIZZA',color:  Colors.blue,type:  VehicleType.Moto);
    Vehicle vehicle3 = Vehicle(country: 'DE', plate:'POLENTA',color:  Colors.orange,type:  VehicleType.SUV);
    Vehicle vehicle4 = Vehicle(country: 'FR', plate:'CREPE',color:  Colors.black,type:  VehicleType.Limo);
    addVehicle(vehicle1);
    addVehicle(vehicle2);
    addVehicle(vehicle3);
    addVehicle(vehicle4);
  }

  MyVehicles() {
  //  addVehicle(Vehicle(country: 'GR', plate:  'ADD',  color: Colors.grey, type: VehicleType.Add));//Adding Add card by default
    init();
  }

  void addVehicle(Vehicle newVehicle) {
    listOfVehicles.forEach((element) => element.preferedVehicle=false) ; //Setting all cards to not default
    listOfVehicles.insert(0,newVehicle); //adding new items in frond (then they appear at the top of the drop down
    _preferredVehicleIdx = 0; //pointing automatically to the last car added
     notifyListeners();
  }

  void deleteVehicle(int idx) {
    print("#cards: ${listOfVehicles.length}");
    if (_preferredVehicleIdx == idx)
      if (idx > 0) //seting the previous card as default card
        PreferedVehicle = idx - 1;
      else if (listOfVehicles.length >
          1) //if deleting 1st card then making next card as default
        PreferedVehicle = 0;
      else
        _preferredVehicleIdx = -1; //no other card in the wallet

    listOfVehicles.removeAt(idx);
    print("#cards: ${listOfVehicles.length}");
    notifyListeners();
  }

  int get preferedVehicle => _preferredVehicleIdx;

  set PreferedVehicle(int i){
    print("Default car: $i");
    listOfVehicles.forEach((element) => element.preferedVehicle =false);
    listOfVehicles[i].preferedVehicle=true;
    _preferredVehicleIdx=i;
    notifyListeners();
  }

}