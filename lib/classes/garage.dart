import 'dart:math';
import 'package:flutter/material.dart';

//Priceslot: it definds a pricing strategy in the form price=flat_rate+excess_time*rate
//a parking garage may have multiple price slot


//pice slot takes relative time!
//each day has up to 4 PriceSlots
class PriceSlot {
  double _flatPrice;
  int _flatPriceDurationInMinutes; //duration of
  double _rate; //parking rate after the flat period
  int _chargePeriodInMinutes = 60; //is it every 15, 30 45 or 60?
  int _parkingstreamCapacity; //how many cars of parkingstream are allowd in this time zone?
  TimeOfDay _startTime; //the start time of the timeslot. This info is relative to the open time! eg if garage opens at 9:00 then 10:30 -> 01:30!
  late TimeOfDay _stopTime; //the end time of the timeslot. This info is not specified in the constuctor but is set such as in order to fill the day. This is set later from one level up
//stop time might be smaller than the start time if it is in the next day!

//constructor
  PriceSlot( {required double flatPrice, required int flatPriceDurationInMinutes, required double rate, required int chargePeriodInMinutes, required int parkingstreamCap, required TimeOfDay startTime})
    //price = _flat_price + "excess time"*rate
    : _flatPrice = flatPrice,
     _flatPriceDurationInMinutes = flatPriceDurationInMinutes,
     _rate = rate,
     _chargePeriodInMinutes = chargePeriodInMinutes,
     _parkingstreamCapacity = parkingstreamCap,
     _startTime = startTime;


//Date_to_double takes a TimeofDate object and returns a double 1:45 -> 1.75
  double time2Double(TimeOfDay date) => date.hour + date.minute / 60;

//returns the price of a vehicle during some time in the interval
//what happens is the duration is longe than the slot's period?
  double getPrice(TimeOfDay tStartParking, TimeOfDay tStopParking) {
      double? t1, t2, dt; //  payment time points. first, last and duration
      int dtInMinutes;

      //first find if the requested time range is outside the start and stop time of this time slot
      if ((time2Double(tStartParking) >
          time2Double(_stopTime)) | (time2Double(tStopParking) <
          time2Double(_startTime)))
        return 0;
      else {
        if (time2Double(tStartParking) < time2Double(_startTime))
          t1 = time2Double(_startTime);
        else
          t1 = time2Double(tStartParking);

        if (time2Double(tStopParking) > time2Double(_stopTime))
          t2 = time2Double(_stopTime);
        else
          t2 = time2Double(tStopParking);

        dt = t2 - t1;
        dtInMinutes = (dt * 60).floor();
      }

      if (dtInMinutes < _flatPriceDurationInMinutes) //compareTo retuens -1, 0 or 1 depending what is bigger
        return _flatPrice;
      else {
        int excessMinutes = (dtInMinutes - _flatPriceDurationInMinutes);
        return _flatPrice +  _rate * (excessMinutes - _chargePeriodInMinutes);
      }
    }

  double get getFlatPrice =>                 _flatPrice;
  int get getFlatPriceDurationInMinutes => _flatPriceDurationInMinutes;
  double get getRate =>                      _rate;
  int get getChargePeriodInMinutes =>      _chargePeriodInMinutes;
  int get getParkingstreamCapacity =>        _parkingstreamCapacity;
  TimeOfDay get getStartTime =>              _startTime;
  TimeOfDay get getStopTime =>               _stopTime;

  set setFlatPrice(double value)                 =>  _flatPrice = value;
  set setFlatPriceDurationInMinutes(int value) =>  _flatPriceDurationInMinutes = value;
  set setRate(double value)                      =>  _rate = value;
  set setChargePeriodInMinutes(int value)      =>  _chargePeriodInMinutes = value;
  set setparkingstreamCapacity(int value)        =>  _parkingstreamCapacity = value;
  set setStartTime(TimeOfDay time)               =>  _startTime = time;
  set setStopTime(TimeOfDay time)                =>  _stopTime = time;
}

enum DayOfWeek {mon, tue, wed, thu, fri, sat, sun} //mon.index = 0, sun.index=6

class ParkingDay{

  // a parking day has many Parking slots defining different payment per day eg. 08.:00-17:00 migh have different cost structure than the 17:00-22:00
  DayOfWeek _dayOfWeek; //1,2,3,4,5,6,7
  TimeOfDay _openingTime; //this is the opening time of the parking (eg. 09:00)
  Duration _duration;  // 23h if the parking will be open for 23 hours

  late List <PriceSlot?> listPricingSlotMain;

  ParkingDay({required DayOfWeek dayOfWeek,required TimeOfDay openingTime,required Duration duration,
    required double flatPrice, required int flatPriceDurationInMinutes,required double rate, required int chargePeriodInMinutes,required int parkingstreamCap})
    :_dayOfWeek=dayOfWeek,
    _openingTime = openingTime,
    _duration = duration
  {
    PriceSlot pSlot;
    pSlot = PriceSlot(flatPrice: flatPrice, flatPriceDurationInMinutes: flatPriceDurationInMinutes ,rate: rate,
        chargePeriodInMinutes: chargePeriodInMinutes, parkingstreamCap: parkingstreamCap,  startTime: TimeOfDay(hour: 0, minute: 0));
    //priseslots have relative time so first instance we have starts at 0 and ends at 0+ parking_day_duration)

    pSlot.setStopTime = TimeOfDay( hour: duration.inHours, minute: duration.inMinutes % 60); //adding by default the first paskingslot that covers all day
    listPricingSlotMain.add(pSlot);
  }

  //to add a pice slot we only specify the start time. then it auto extends till the end of the day or till the next slot
  int addPriceSlot ({required double flatPrice, required int flatPriceDurationInMinutes, required double rate, required int chargePeriodInMinutes, required int parkingstreamCap, required TimeOfDay startTime}) {
    if (listPricingSlotMain.length > 3) {
      print('day has already 4 parking slots. Please remove one first');
      return -1;
    }
    PriceSlot pslot;
    pslot = PriceSlot(flatPrice: flatPrice,
        flatPriceDurationInMinutes: flatPriceDurationInMinutes,
          rate: rate,
        chargePeriodInMinutes: chargePeriodInMinutes,
        parkingstreamCap: parkingstreamCap,
        startTime: startTime);

    listPricingSlotMain.add(pslot);
    listPricingSlotMain.sort((a, b) => a!=null && b!=null ? a._startTime.hour.compareTo(b._startTime.hour):0); //shorting

    //setting stop time equal the next oject's start time
    for (int i = 0; i < listPricingSlotMain.length - 1; i++)
      listPricingSlotMain[i]?.setStopTime = listPricingSlotMain[i + 1]!.getStartTime;

    listPricingSlotMain.last!.setStopTime = TimeOfDay( hour: _duration.inHours,  minute: _duration.inMinutes %  60); //seting end time of last time slot to clsing time
   return listPricingSlotMain.length;
  }

 int deletePriceSlot(int idx) {
   listPricingSlotMain.removeAt(idx);
    return 0;
 }
}


class GarageCapacity {
  int? _total;     //total cars capacity for the parking
  int? _parkingstream;   //total parkingstream cars allocated ina  parking
  int? _totalParked; //total cars parked now in the parking
  int? _parkingstreamParked; //total "parkingstream controlled" cars parked now in the parking

  GarageCapacity({int? total,   int? parkingstream,  int? totalParked,  int? parkingstreamParked }){
    _total=total;
    _parkingstream=parkingstream;
    _totalParked=totalParked;
    _parkingstreamParked=parkingstreamParked;
  }
  get getTotal=>_total;
  get getParkingstream=>_parkingstream;
  get getTotalParked=>_totalParked;
  get getParkingstreamParked=>_parkingstreamParked;
  set setTotal(int value)=>_total=value;
  set setParkingstream(int value)=>_parkingstream=value;
  set setTotalParked(int value)=>_totalParked=value;
  set setParkingstreamParked(int value)=>_parkingstreamParked=value;


}

class SizeCheck{
  double? _maxWidth=100.0;
  double? _maxHeight=100.0;
  double? _maxLength=100; //100m
  double? _priceScaleSizeParam=1; //price scaling based on size
  double? _priseScaleBikeParam=1; //priced scaling based on motorcycle vs other behicle

  SizeCheck( {double? maxWidth,  double? maxHeight,  double? maxLngth,
              double? priceScaleSizeParam,   double? priseScaleBikeParam})  {
     _maxWidth=_maxWidth;
     _maxHeight=_maxHeight;
     _maxLength=_maxLength; //100m
     _priceScaleSizeParam= priceScaleSizeParam;
     _priseScaleBikeParam= priseScaleBikeParam;
  }
  set setMaxWidth(double data) =>  _maxWidth = data;
  set setMaxHeight(double data) => _maxHeight = data;
  set setMaxLength(double data) => _maxLength= data;
  set setPriceScaleSizeParam(double data) => _priceScaleSizeParam = data;
  set setPriseScaleBikeParam(double data) => _priseScaleBikeParam = data;
  get getMaxWidth => _maxWidth;
  get getMaxHeight => _maxHeight;
  get getMaxLngth => _maxLength;
  get getPriceScaleSizeParam => _priceScaleSizeParam ;
  get getPriseScaleBikeParam => _priseScaleBikeParam ;

  double priceCalc(){
    print('we should have a clever algorithm here');
    return 1.0;
  }
}

enum Country {GR, IT}

class Address{
  String _street;
  int? _number;
  String _city;
  int _zipCodec;
  Country? _country = Country.GR;

  Address( {required String street, int? num, required String city, required int zipCodec, required Country country})
     : _street = street,
      _number = num,
      _city = city,
      _zipCodec = zipCodec,
      _country = country;

  set setSreet(String data) => _street=data;
  set setNumber(int data) => _number=data;
  set setCity(String data) => _city=data;
  set setZipCodec(int data) => _zipCodec=data;
  set setCountry(Country data) => _country=data;
  get getStreet => _street;
  get getNumber => _number;
  get getCity => _city;
  get getZipCodec => _zipCodec;
  get getCountry => _country;
}

class Contact{
  String? _email;
  int _phone;
  // add 2nd phone, emergency
  String? _website ;
  Contact({String? email, required int phone, String?web})
    :_email = email,
    _phone =phone,
    _website =web ;


  get getEmail =>_email;
  get getPhone => _phone;
  get getWebsite => _website;
  set setEmail(String data) =>_email=data;
  set setPhone(int data) => _phone=data;
  set setWebsite(String data) => _website=data;
}

class Social{
  String? _facebook;
  String? _twitter;
  Social({ String? fbook, String? twitter}){
    _facebook=fbook;
    _twitter=twitter;
  }
  get getFbook =>_facebook;
  get getTwitter => _twitter;
  set setFbook(String data) =>_facebook=data;
  set setTwitter(String data) => _twitter=data;

}
class Garage {
  String _name; //name
  bool _offHoursParking = false; //is the car allowd to park after the garage is closed?

  GarageCapacity? _capacity;
  Address _address;
  Point _coordinates;
  Social? _social;
  Contact? _contact;

  late List<ParkingDay> listWeeklySchedule ; //this is a list containing 7 Parkingday objects; PArkingday objects define the pricing



  Garage({required String garageName, required Address address, required Point, coordinates})
  : _name = garageName,
    _address=address,
    _coordinates=coordinates;


  void addDay({required DayOfWeek dayOfWeek, required TimeOfDay openingTime,required Duration duration,
    required double flatPrice, required int flatPriceDurationInMinutes, required double rate, required int chargePeriodInMinutes, required int parkingstreamCap}){

   ParkingDay pday;
   pday=ParkingDay(dayOfWeek: dayOfWeek, openingTime: openingTime, duration: duration,
       flatPrice: flatPrice, flatPriceDurationInMinutes: flatPriceDurationInMinutes, rate: rate,
   chargePeriodInMinutes: chargePeriodInMinutes, parkingstreamCap: parkingstreamCap);

   if (listWeeklySchedule.length ==0)  //if list is empty then copy the fist day 7 times. else copy the day to the right location:)
     for (int i=0; i<7; i++)
       listWeeklySchedule.add(pday);
   else
     listWeeklySchedule[dayOfWeek.index]=pday;  //this overides a new PArkingday object into it's right place

  }

  get getName => _name;
  get getOffHoursParking => _offHoursParking;
  set setName(String name) => _name=name;
  set setOffHoursParking(bool value) => _offHoursParking=value;

}


