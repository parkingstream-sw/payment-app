//need to define the authentication.........SOS SOS
import 'package:flutter/material.dart';

import 'package:payment_app/classes/vehicle.dart';
import 'package:payment_app/classes/payment.dart';
import 'package:payment_app/classes/timestamp.dart';

const String avatar_path='assets/avatar/';
enum  Avatar {  addUser, genderX, heart, lipstick, mustache, nerd, plain, pic }

String getAvatarName(Avatar myAvatar){
  return myAvatar.toString().substring(myAvatar.toString().indexOf('.')+1);
}


class UserId extends ChangeNotifier{
late String _firstName;
late String _lastName;
late String _email;
late int _id;
late String _password; //???????? authentication
Avatar _avatar;
String? _picture;  //to store the picture on string format/blob/base64

UserId()
:_avatar=Avatar.addUser;


set firstName(String val) => _firstName=val;
set lastName(String val) => _lastName=val;
set email(String val) => _email=val;

set password(String password) => _password=password;


set avatar(Avatar val) {
  _avatar = val;
  notifyListeners();
}


  String get firstName =>  _firstName;
  String get lastName =>  _lastName;
  String get email =>  _email;
  int get id => _id;
  String get AvatarPath => avatar_path + AvatarName + '.png';
  Avatar get UserAvatar => _avatar;

  String get AvatarName{
    return getAvatarName(_avatar);
  }


}

class User{
  Wallet? _wallet;
  TimeStamp? _timestamp;
  MyVehicles? _vehicles;
  UserId? _userId;
  User();
}

