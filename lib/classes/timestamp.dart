import 'dart:core';
import 'dart:async';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:payment_app/functions.dart';

///describes the session of a user
class   TimeStamp extends ChangeNotifier{

  ///Start time is set to automatic. If set to manual then it has to be in the future (unconstrained)
  ///Duration @init is set to 1h automatically, cannot be smaller than 5min
  ///Stop time --> is derived from Duration
  ///timelapse . After the end of the end of the session timelapsed is the total parking time
  ///_setDuration --> normaly duration is the default mode but user can schedule start time
  ///

  bool _hasStarted;
  bool _hasStopped;
  bool _setStartTimeAutomatically;
  bool _durationMode; //
  bool _timeLaplseDirectionUp; //the display timer should count up or down?

  DateTime _startTime;
  late Duration _duration; // Target Duration = _stopTime - _startTime
  late DateTime _stopTime; // Target Stop Time = _startTime + _duration, derived from duration
  Duration _timelapse; //timelapse . After the end of the end of the session toimelapsed is the total parking time

  late Stream <String> _counterStream ;
  StreamController<String> _streamController= StreamController<String>();
  var stopwatch= Stopwatch();

  TimeStamp ()
     : _hasStarted = false ,
      _hasStopped = false  ,
      _setStartTimeAutomatically = true,
      _durationMode=true,
      _timeLaplseDirectionUp = true,
      _startTime = DateTime.now(),
      _duration= Duration(hours: 1), //_targetDuration = _tStop - _tStart
      _timelapse = Duration(minutes:0)   //
      {
        print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        print("has started?: $_hasStarted");

  //  _counterStream = ;
        _streamController.add("-");
  } //initialization is done above

  bool get HasStarted  {
  print ("called get HasStarted");
  return _hasStarted ;
  }
  bool get HasStopped   => _hasStopped ;
  bool get SetStartTimeAutomatically => _setStartTimeAutomatically;
  bool get DurationMode => _durationMode;
  bool get TimeLaplseDirectionUp => _timeLaplseDirectionUp;

  DateTime get StartTime=> _startTime ;
  Duration get TargetDuration => _duration;
  DateTime get StopTime {
    _stopTime = StartTime.add(_duration);  //Duration cannot be negative (look in the durstion setter)
    return _stopTime;
  }
  Duration get TimeLapse {
    if (_hasStarted & ! _hasStopped){   //this is during parking time
      _timelapse = DateTime.now().difference(_startTime);
      return _timelapse;
    }
    else if(_hasStopped)   //this is after the end of the session. Here _timelapsed is set when stop parking is set
      return _timelapse;
    else //this is is parking has not start yet
      return Duration(microseconds: 0);
  }


  void startSession() { //call this to start the session
      StartTime=DateTime.now();
      _hasStarted = true;
      _hasStopped = false;
      stopwatch.start();
      notifyListeners();
  }
  void stopSession()    { //call this to stop the session
    _hasStopped = true;
    _timelapse=DateTime.now().difference(_startTime);
    stopwatch.stop();
    notifyListeners();
  }

  set StartTime(DateTime val) {   //_startTime cannot be null or be set to a past time
    if(val!=null  ) {
      _setStartTimeAutomatically=false;
      _startTime = val;
    }
    else {
      print('start time has invalid format :(. So it is set to start automatically');
      _setStartTimeAutomatically = true;
    }
      notifyListeners();
  }
  set TargetDuration(Duration value){ //duration cannot be smaller than 5min
     _durationMode=true;

    if (value.inMinutes < 5){
      print('error: Duration cannot be smaller than 5 min. It is set automatically to 5 min instead');
      _duration = Duration(minutes: 5);
    }
    else
      _duration=value;

    _stopTime=_startTime.add(_duration);
    notifyListeners();
  }

  set StopTime(DateTime value){ //stop time cannot be smaller than 5min after start time
    _durationMode=false;
    _duration =value.difference(_startTime);

    if (_duration.inMinutes < 5){
      print('error: Duration cannot be smaller than 5 min. It is set automatically to 5 min instead');
      _stopTime = _startTime.add(Duration(minutes: 5));
      _duration = Duration(minutes: 5);
    }
    else
      _stopTime=value;


    notifyListeners();
  }


  set SetStartTimeAutomatically(bool val) {
    _setStartTimeAutomatically=val;
    notifyListeners();
  }


  set DurationMode(bool val) {
    _durationMode=val;
    notifyListeners();
  }

  set TimeLaplseDirectionUp(val) {
    _timeLaplseDirectionUp = val;
    print("_countDown: $_timeLaplseDirectionUp");
  }

 //
 //  String CountingUpDown_string(){
 //
 //     if(!_hasStarted )
 //        return "ZZ:ZZ";
 // //   else if(_hasStarted && _hasStopped)
 // //     return "ZZ:ZZ";
 //    else
 //     if (_timeLaplseDirectionUp)  //counting UP
 //       return Duration2StringAdjustableHHmmORmmss(DateTime.now().difference(_startTime));
 //     else  //counting down
 //       return Duration2StringAdjustableHHmmORmmss(_stopTime.difference(DateTime.now()) );
 //  }

  String CountingUpDown_string2(){


    if(!_hasStarted )
      return "--";
    //   else if(_hasStarted && _hasStopped)
    //     return "ZZ:ZZ";
    else
    if (_timeLaplseDirectionUp)  //counting UP
      return Duration2StringAdjustableHHmmORmmss(stopwatch.elapsed);
    else  //counting down
      return Duration2StringAdjustableHHmmORmmss(_stopTime.difference(DateTime.now()) );
  }



  Stream<String> getStopWatchStream() {


      _counterStream = Stream.periodic(Duration(seconds: 1), (_) =>
          CountingUpDown_string2());

    return     _counterStream;
  }



}
