import 'dart:async';
import 'package:flutter/material.dart';


class StopWatchService extends ChangeNotifier{
  String _timeToDisplay;
  Stopwatch _stopWatch ;
  final _duration = const Duration(seconds: 1);
  late StreamController<String> _stopWatchStreamController;

  StopWatchService()
    :_timeToDisplay = '00:00:00',
    _stopWatch = Stopwatch();


  void start() {
    _stopWatch.start();
    startTimer();
  }

  void startTimer() {
    Timer(_duration, keepRunning);
    print ("timer is running..........");
  }

  void keepRunning() {
    if (_stopWatch.isRunning) {
      startTimer();
    }

    var hours = _stopWatch.elapsed.inHours.toString().padLeft(2, "0");
    var minutes = (_stopWatch.elapsed.inMinutes % 60).toString().padLeft(2, "0");
    var seconds = (_stopWatch.elapsed.inSeconds % 60).toString().padLeft(2, "0");

    _timeToDisplay = hours + ':' + minutes + ':' + seconds;
    if (_stopWatch.isRunning) {
      _stopWatchStreamController.add(_timeToDisplay);
    }
    else {
      _stopWatchStreamController.close();
    }

    // timeInSec = swatch.elapsed.inSeconds;
  }



  void stop() {
    print('ride service stop ${_stopWatch.isRunning}');
    _stopWatch.stop();
    // stopWatchStreamController.close();
    print('ride service stop 1 - ${_stopWatch.isRunning}');
  }

  Stream<String> getStopWatchStream() {

    return _stopWatchStreamController.stream;


  }

}