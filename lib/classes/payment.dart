import 'package:flutter/material.dart';

enum PaymentType  { Visa, Mastercard, PayPal, Google, Apple, Add}

//This is a PaymentType -> Widget (and not IconData)
Map<PaymentType,String> paymentPicPathMap  = {
  PaymentType.Visa: 'assets/payment/visa_symbol.png',
  PaymentType.Mastercard: 'assets/payment/mastercard_symbol.png',
  PaymentType.PayPal:  'assets/payment/paypal_symbol.png' ,
  PaymentType.Google:  'assets/payment/google_symbol.png' ,
  PaymentType.Apple: 'assets/payment/apple_symbol.png' ,
  PaymentType.Add:  'assets/payment/add_symbol.png'
};


class PaymentMethod{
  PaymentType _type;  //Mastercard
  Color _color=Colors.white; //defualt color for paypal , google, apple pay
  DateTime _expDate;
  int? _ccv; //X
  int? _cardNumber; // X
  int? _lastDigits;  //L
  String _name; //X
  bool _valid=true;
  bool _defaultCard=true;

  PaymentMethod.card({required  PaymentType type, required Color color,  required DateTime expDate,   required String name, required int cardNumber, required int ccv})
   :  _expDate=expDate,
       _cardNumber=cardNumber,
       _ccv=ccv,
      _name=name,
      _type=type,
      _color=color
  {
    if(_cardNumber==null)
      _lastDigits=null;
    else
      _lastDigits=_cardNumber!%10000 ;
  }

  PaymentMethod.alt({required  PaymentType type,  required DateTime expDate,   required String name, })
      :  _expDate=expDate,
        _name=name,
        _type=type;


  set defaultCard(bool val) => _defaultCard=val;


  bool get isExpired{  //returns true if card is expired
    var now = new DateTime.now();
    return _expDate.isAfter(DateTime.now()) ;
  }

  bool get isValid => _valid; // is the card number correctly? is there a negative signal back from the bank

  Color get color => _color;
  String get cardName =>  _name;
  PaymentType get type =>  _type;
  int? get lastDigits =>  _lastDigits;
  bool get defaultCard =>  _defaultCard;
  DateTime get expDate => _expDate;

  set isValid(bool val) => _valid=val;

  String get imagePath {
    return  paymentPicPathMap[_type]!;
 }

}


//to ++add++ all the discolunts per parking as a List !!!!!!!!!!!!!!!!   // currency?
class Wallet extends ChangeNotifier{

  double _balance=0; //account balance
  double _discounts=0;  //total discounts: sum of all the offers

  //refill
  bool   _enableRefill = false; // if true then auto refill using specified card
  double _amountRefill = 10; //automatic Refill by 10euros
  double _thresholdRefill = 3; //if (_balance< 3euros) then _balance+=_automaticRefillAmount

  //means of payment
  List<PaymentMethod> listMeansOfPayment =<PaymentMethod>[] ;
  int _defaultCardIdx=-1;   //initialize to -1 because there is no mean of payment initially


  void init(){
    PaymentMethod card1 = PaymentMethod.card( type: PaymentType.Visa ,   color:  Colors.green,   expDate: DateTime(1989, 11, 30), name:  "Pateropoulos",cardNumber: 123456781234,  ccv:  123,  );
    PaymentMethod Pm1 = PaymentMethod.card( type:PaymentType.Mastercard ,  color: Colors.red, expDate: DateTime(2025, 10, 31),  name: "MAssive Attack", cardNumber: 123456789456,  ccv:  313  );
    PaymentMethod Pmgoog = PaymentMethod.alt( type:PaymentType.PayPal, expDate: DateTime(2021, 06, 31),name: "pateropoulos");
    PaymentMethod Pm4 = PaymentMethod.alt( type:PaymentType.Google, expDate: DateTime(2021, 06, 31),name: "pateropoulos");
    addCard(card1);
    addCard(Pmgoog);
    addCard(Pm1);
    addCard(Pm4);
  }
  Wallet(){
 //   addCard(PaymentMethod.alt(type: PaymentType.Add, expDate: DateTime(2025, 10, 31), name: " "));  //Adding Add card by default
    print('Wallet was created');
    init();
    notifyListeners();
  }

  void autoRefill() {
    if(_enableRefill)
      balance += _amountRefill;
    notifyListeners();
  }

  void refill({required double amount, required double threshold, required int cardID}) {
    balance += amount > 0 ? amount : 0;
    notifyListeners();
  }

  void addCard(PaymentMethod newCard) {
    listMeansOfPayment.forEach((element) => element.defaultCard=false) ; //Setting all cards to not default
    listMeansOfPayment.insert(0,newCard);  //adding new items in frond (then they appear at the top of the drop down
    _defaultCardIdx=0; //last added card is the default
    notifyListeners();
  }

  void deleteCard(int idx) {
    print("#cards: ${listMeansOfPayment.length}");
    if (_defaultCardIdx ==idx)
      if(idx>0)                 //seting the previous card as default card
        _defaultCardIdx = idx-1;
      else
        if(listMeansOfPayment.length>1)  //if deleting 1st card then making next card as default
          _defaultCardIdx = 0;
        else
          _defaultCardIdx =-1;    //no other card in the wallet

    listMeansOfPayment.removeAt(idx);
    print("#cards: ${listMeansOfPayment.length}");
    notifyListeners();
  }

  set balance(double amount) {
    _balance +=  amount;
    notifyListeners();
  }
  set discounts(double amount) {
    _discounts += amount;
    notifyListeners();
  }

  set refillAmount (double val) {
    if (val>0)
      _amountRefill = val;
    notifyListeners();
  }

  set thresholdRefill (double val) {
    if (val>0)
      _thresholdRefill = val;
    notifyListeners();
  }

  set enableRefill (bool val) {
    _enableRefill = val;
    notifyListeners();
  }

  set defaultCard(int i){
    print("Default card: $i");
    listMeansOfPayment.forEach((element) => element.defaultCard=false);
    listMeansOfPayment[i].defaultCard=true;
    _defaultCardIdx=i;
    notifyListeners();
  }

  int get defaultCard => _defaultCardIdx;
  bool get enableRefill => _enableRefill;
  double get refillAmount => _amountRefill;
  double get thresholdRefill => _thresholdRefill;
  double get balance => _balance;
  double get discounts => _discounts    ;

}

class Payment {

  Payment(double amount){

    print('you need to add the payment api here');

  }

}